# Field Prefix

Customize the field prefix or remove the default prefix **field_**

Install this module and visit `admin/config/field_prefix/setting`
